target('omni')
    set_default(true)
    set_kind('binary')
    set_objectdir('.xmake/build/obj')
    set_dependir('.xmake/build/dep')
    set_targetdir('../bin')
    set_optimize('fastest')

    if is_os('windows') then
        if is_mode('debug') then
            set_symbols('debug')
            add_defines('DEBUG')
        else
            set_symbols('hidden')
            set_strip('all')
        end
        add_files('omni.rc')
        add_links('lua51', 'microhttpd', 'ws2_32', 'ole32', 'user32')
    elseif is_os('macosx') then
        add_links('lua51_macos', 'microhttpd_macos', 'pthread')
    else
        add_links('lua51', 'microhttpd', 'pthread', 'uuid', 'dl')
    end

    add_includedirs('backend/luajit/include', 'backend/microhttpd/include')
    add_linkdirs('backend/luajit/lib', 'backend/microhttpd/lib') 

    add_headers('core/*.h', 'utils/*.h')
    add_files('omni.cc', 'core/*.cc', 'utils/*.cc', 'apis/*.cc', {languages = 'cxx11'})
target_end()

